import org.apache.commons.io.IOUtils;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.HttpClients;
import org.apache.kafka.clients.producer.KafkaProducer;
import org.apache.kafka.clients.producer.Producer;
import org.apache.kafka.clients.producer.ProducerRecord;
import org.json.simple.JSONObject;
import org.json.simple.JSONValue;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.Properties;

public class Theodoinhom {
    public static void main(String[] args) throws IOException {
        // The Kafka Topic
        String topicName = "theodoinhom";

        // create instance for properties to access producer configs
        Properties props = new Properties();
        //Assign localhost id
        props.put("bootstrap.servers", "localhost:9092");
        //Set acknowledgements for producer requests.
        props.put("acks", "all");
        //If the request fails, the producer can automatically retry,
        props.put("retries", 0);
        //Specify buffer size in config
        props.put("batch.size", 16384);
        //Reduce the no of requests less than 0
        props.put("linger.ms", 1);
        //The buffer.memory controls the total amount of memory available to the producer for buffering.
        props.put("buffer.memory", 33554432);
        props.put("key.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");
        props.put("value.serializer",
                "org.apache.kafka.common.serialization.StringSerializer");

        //Define a Kafka Producer
        Producer<String, String> producer = new KafkaProducer<String, String>(props);

        //Get feeds of the theodoinhom.com
        ArrayList<String> feeds;

        feeds = getFeeds();

        int i = 1;
        for(String feed: feeds) {
            //Send the feed to a particular Kafka Topic
            producer.send(new ProducerRecord<String, String>(topicName, Integer.toString(i), feed));
            i ++;
        }
        System.out.println("Message sent successfully");
        producer.close();
    }

    public static ArrayList<String> getFeeds() throws IOException {
        HttpGet httpGet = new HttpGet("https://api.theodoinhom.com/api/post/feed?page=1&source=all");
        HttpClient client = HttpClients.createDefault();
        HttpResponse httpResponse = client.execute(httpGet);
        String content = IOUtils.toString(httpResponse.getEntity().getContent(), StandardCharsets.UTF_8);
        Object obj = JSONValue.parse(content);
        JSONObject jsonObject = (JSONObject) obj;

        ArrayList<Object> data = (ArrayList<Object>) jsonObject.get("data");

        ArrayList<String> feedLists = new ArrayList<String>();
        for (Object tmp : data) {
            JSONObject tmpContent = (JSONObject) tmp;
            String message = (String) tmpContent.get("message");
            String shareTitle = (String) tmpContent.get("share_title");

            feedLists.add("Share Title: " + shareTitle + "\n" + "Message: " + message + "\n-----------------------------------");
        }

        return feedLists;
    }
}

